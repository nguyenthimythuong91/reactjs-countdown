import { useEffect, useRef, useState } from 'react';
import './App.css';

function App() {
  const [valueInput, setValueInput] = useState('');
  const [countdownTime, setCountdownTime] = useState<number>(0);
  const [countDown, setCountDown] = useState<number>(0);
  const [isCounting, setIsCounting] = useState(false);
  const interval = useRef<NodeJS.Timer>();

  //Countdown
  useEffect(() => {
    if (isCounting) {
      let i = 0;
      interval.current = setInterval(() => {
        i = i + 1;
        setCountDown(countdownTime - i);
      }, 1000);
    } else {
      clearInterval(interval.current);
      interval.current = undefined;
    }
    return () => clearInterval(interval.current);
  }, [countdownTime, isCounting]);

  // Clear countdown on ending time
  useEffect(() => {
    if (countDown < 1) {
      clearInterval(interval.current);
      interval.current = undefined;
    }
  }, [countDown]);

  const onChangeInput = (e: any) => {
    const time: string = e.target.value;
    setValueInput(time);
  };

  const onBlurInput = (e: any) => {
    const time: string = (e.target.value);
    const seconds = convertTimeToSeconds(time);
    setCountDown(seconds);
    setCountdownTime(seconds);
  };

  const onStartCountDown = () => {
    setIsCounting(true);
  }
  const onStopCountDown = () => {
    setIsCounting(false);
    setCountdownTime(countDown);
  }
  const onResetCountDown = () => {
    const seconds = convertTimeToSeconds(valueInput);
    setCountDown(seconds);
    setCountdownTime(seconds);
    setIsCounting(false);
  }

  const convertTimeToSeconds = (value: string) => {
    const minutes = Number((value.split(':'))[0]) * 60;
    const second = Number((value.split(':'))[1]);
    return minutes + second;
  };

  const getReturnTime = (countDown: number) => {
    const minutes = Math.floor(countDown / 60);
    const seconds = Math.floor(countDown % 60);
    return `${('0' + minutes).slice(-2)}:${('0' + seconds).slice(-2)}`;
  };

  return (
    <div className="App">
      <h1>Countdown</h1>
      <div>
        <input disabled={countdownTime > 0} onChange={onChangeInput} onBlur={onBlurInput} value={valueInput} />
        <button disabled={isCounting} onClick={onStartCountDown}>Start</button>
      </div>
      <div style={{ display: countdownTime > 0 ? 'block' : 'none' }}>
        <div>{getReturnTime(countDown)}</div>
        <button disabled={isCounting} onClick={onStartCountDown}>Start</button>
        <button onClick={onStopCountDown}>Stop</button>
        <button onClick={onResetCountDown}>Reset</button>
      </div>
    </div>
  );
}

export default App;
